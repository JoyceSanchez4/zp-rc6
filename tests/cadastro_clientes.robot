***Settings***
Documentation       Cadastro de clientes

Resource        ../resources/base.robot

Test Setup          Login Session
Test Teardown       Finish Session

***Test Cases***
Novo cliente
    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:
    ...         Bon Jovi        00000001406     Rua dos bugs, 1000      11999999999
    Então devo ver a notificação:   Cliente cadastrado com sucesso!

Campos obrigatórios
    [tags]      temp
    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:
    ...     ${EMPTY}    ${EMPTY}    ${EMPTY}    ${EMPTY}
    Então devo ver mensagens informando que os campos do cadastro de clientes são obrigatórios

Nome é obrigatório
    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:
    ...         ${EMPTY}        00000001406     Rua dos bugs, 1000      11999999999
    Então devo ver a notificação de nome obrigatório

Cpf é obrigatório
    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:
    ...         Bon Jovi        ${EMPTY}     Rua dos bugs, 1000      11999999999
    Então devo ver a notificação de cpf obrigatório

Endereço é obrigatório
    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:
    ...         Bon Jovi     00000001406        ${EMPTY}      11999999999
    Então devo ver a notificação de Endereço obrigatório

Telefone é obrigatório
    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:
    ...         Bon Jovi     00000001406        Rua dos bugs, 1000     ${EMPTY} 
    Então devo ver a notificação de Telefone obrigatório





